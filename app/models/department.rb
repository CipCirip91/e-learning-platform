class Department < ActiveRecord::Base
  belongs_to :study_year

  has_many :courses, dependent: :destroy
  accepts_nested_attributes_for :courses, reject_if: :all_blank, allow_destroy: true

  def available_teachers
    User.where(role: 'prof')
  end

end
