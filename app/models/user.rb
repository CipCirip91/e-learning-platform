class User < ActiveRecord::Base
  ROLES = %w(admin prof student)

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # validates :name, presence: true

  has_many :student_course_memberships, foreign_key: 'student_id'
  has_many :participating_courses, class_name: 'Course', through: :student_course_memberships, source: :course

  has_many :course_teacher_memberships, foreign_key: 'teacher_id'
  has_many :teaching_courses, class_name: 'Course', through: :course_teacher_memberships, source: :course

  scope :students, -> { where(role: 'student') }

  has_many :student_grades, foreign_key: 'student_id'

  has_many :sent_messages, class_name: "Message", foreign_key: :sender_id, dependent: :destroy
  has_many :messages, class_name: "Message", foreign_key: :receiver_id, dependent: :destroy

  def admin?
    role == 'admin'
  end

  def my_courses
    if role == 'student'
      participating_courses
    elsif role == 'prof'
      teaching_courses
    elsif role == 'admin'
      Course.all
    end
  end

  def to_label
    "#{name} (#{email})"
  end

end
