class CourseAttachment < ActiveRecord::Base
  mount_uploader :file, CourseChapterAttachmentUploader

  belongs_to :chapter
  belongs_to :course

  before_save :set_course

  def set_course
    self.course = chapter.course
  end

end
