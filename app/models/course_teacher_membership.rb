class CourseTeacherMembership < ActiveRecord::Base

  belongs_to :course
  belongs_to :teacher, class_name: 'User', foreign_key: 'teacher_id'

  validates :teacher_id, uniqueness: { scope: :course_id }

  after_create :message_teachers

  def message_teachers
    Message.send_message messageable: self, sender: nil, receiver: self.teacher, action: 'create'
  end
end
