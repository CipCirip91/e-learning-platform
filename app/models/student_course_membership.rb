class StudentCourseMembership < ActiveRecord::Base
  belongs_to :student, class_name: 'User', foreign_key: 'student_id'
  belongs_to :course

  after_save :ensure_student_grade
  after_create :message_students

  validates :student, uniqueness: { scope: :course }

  def ensure_student_grade
    StudentGrade.find_or_create_by(student_id: self.student_id, course_id: self.course_id)
  end

  def message_students
    Message.send_message messageable: self, sender: nil, receiver: self.student, action: 'create'
  end
end
