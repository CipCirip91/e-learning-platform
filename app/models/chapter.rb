class Chapter < ActiveRecord::Base

  acts_as_commentable

  belongs_to :course
  has_many :course_attachments

  accepts_nested_attributes_for :course_attachments, allow_destroy: true

  validates :name, uniqueness: { scope: :course_id }

end
