class Ability
  include CanCan::Ability

  def initialize(user)
    # can :manage, :all
    user ||= User.new

    if user.role == 'admin'
      can :manage, :all
      cannot :destroy, User, id: user.id
    elsif user.role == 'prof'
      can :manage, Chapter do |chapter|
        user.teaching_courses.include? chapter.course
      end
      can :update, StudentGrade do |student_grade|
        user.teaching_courses.include? student_grade.course
      end
    end
  end
end
