class Message < ActiveRecord::Base
  belongs_to :messageable, polymorphic: true
  belongs_to :sender,  class_name: 'User'
  belongs_to :receiver,  class_name: 'User'

  after_create :update_receiver_bubble

  def self.send_message(options={})
    receivers = [*options[:receiver]]

    receivers.each do |user|
      message = user.messages.new
      message.sender = options[:sender]
      message.messageable = options[:messageable]
      message.action = options[:action]
      message.save
    end
  end

  def update_receiver_bubble
    self.receiver.update_attribute(:has_notifications, true)
  end
end
