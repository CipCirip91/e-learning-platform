class StudyYear < ActiveRecord::Base

  has_many :departments, dependent: :destroy
  has_many :courses, through: :departments, source: :courses
  has_many :students, through: :courses, source: :students

end
