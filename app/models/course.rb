class Course < ActiveRecord::Base

  acts_as_commentable

  belongs_to :department
  belongs_to :study_year

  has_many :course_teacher_memberships, dependent: :destroy
  has_many :teachers, through: :course_teacher_memberships, source: :teacher

  has_many :student_course_memberships, foreign_key: 'course_id'
  has_many :students, through: :student_course_memberships , source: :student

  has_many :chapters
  has_many :course_attachments, through: :chapters, source: :course_attachments

  has_many :student_grades

  def available_students
    User.where(role: 'student').where.not(id: study_year.students.where.not(id: self.students))
  end

  validates :title, presence: true, uniqueness: { scope: :study_year_id }
  validates :teacher_ids, length: { minimum: 1, :message => 'Select at least one teacher' }

end
