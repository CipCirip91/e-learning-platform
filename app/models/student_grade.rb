class StudentGrade < ActiveRecord::Base

  belongs_to :student, class_name: 'User', foreign_key: 'student_id'
  belongs_to :course

  validates :student_id, uniqueness: { scope: :course_id }

  after_update :message_student

  def message_student
    Message.send_message messageable: self, sender: nil, receiver: self.student, action: 'update'
  end

end
