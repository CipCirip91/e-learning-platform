$document = $(document)

$document.on 'ready page:load', ->
  $('[rel~=tooltip]').tooltip()
  $('select').select2()
  $document.on 'cocoon:after-insert', -> $('select').select2()
  $(".best_in_place").best_in_place()
  $('.navbar-default').dblclick ->
    $('.v-hidden').removeClass('v-hidden')
  $document.on 'click', '.activate-comment-reply', ->
    $(this).closest('.chat-box-timeline').find('.chat-box-timeline-footer').toggleClass('hidden')

$document.on 'ready', -> NProgress.start()
$document.on 'page:change', -> NProgress.done()
$document.on 'page:restore', -> NProgress.remove()