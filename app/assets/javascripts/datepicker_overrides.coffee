$.fn.datetimepicker.DPGlobal.headTemplate =
  '<thead>'+
  '<tr>'+
  '<th class="prev"><i class="icm icm-arrow-left"/></th>'+
  '<th colspan="5" class="switch"></th>'+
  '<th class="next"><i class="icm icm-arrow-right2"/></th>'+
  '</tr>'+
  '</thead>'

$.fn.datetimepicker.DPGlobal.template =
  '<div class="datetimepicker">'+
  '<div class="datetimepicker-minutes">'+
  '<table class=" table-condensed">'+
  $.fn.datetimepicker.DPGlobal.headTemplate+
  $.fn.datetimepicker.DPGlobal.contTemplate+
  $.fn.datetimepicker.DPGlobal.footTemplate+
  '</table>'+
  '</div>'+
  '<div class="datetimepicker-hours">'+
  '<table class=" table-condensed">'+
  $.fn.datetimepicker.DPGlobal.headTemplate+
  $.fn.datetimepicker.DPGlobal.contTemplate+
  $.fn.datetimepicker.DPGlobal.footTemplate+
  '</table>'+
  '</div>'+
  '<div class="datetimepicker-days">'+
  '<table class=" table-condensed">'+
  $.fn.datetimepicker.DPGlobal.headTemplate+
  '<tbody></tbody>'+
  $.fn.datetimepicker.DPGlobal.footTemplate+
  '</table>'+
  '</div>'+
  '<div class="datetimepicker-months">'+
  '<table class="table-condensed">'+
  $.fn.datetimepicker.DPGlobal.headTemplate+
  $.fn.datetimepicker.DPGlobal.contTemplate+
  $.fn.datetimepicker.DPGlobal.footTemplate+
  '</table>'+
  '</div>'+
  '<div class="datetimepicker-years">'+
  '<table class="table-condensed">'+
  $.fn.datetimepicker.DPGlobal.headTemplate+
  $.fn.datetimepicker.DPGlobal.contTemplate+
  $.fn.datetimepicker.DPGlobal.footTemplate+
  '</table>'+
  '</div>'+
  '</div>';