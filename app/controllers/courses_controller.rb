class CoursesController < ApplicationController

  def index
    redirect_to admin_study_years_path if current_user.admin?
    @courses = current_user.my_courses
  end

end