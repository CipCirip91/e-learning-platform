class ChaptersController < ApplicationController

  before_action :load_course

  def index

  end

  def new
    @chapter = @course.chapters.build
  end

  def show
    @chapter = @course.chapters.find(params[:id])
  end

  def create
    @chapter = @course.chapters.new(chapter_params)
    authorize! :create, @chapter
    if @chapter.save
      message messageable: @chapter, sender: current_user, receiver: @chapter.course.students, action: 'create'
      redirect_to course_chapters_path(@course), notice: 'Chapter was created'
    else
      render 'new'
    end
  end

  def update
    @chapter = @course.chapters.find(params[:id])
    authorize! :update, @chapter
    if @chapter.update(chapter_params)
      message messageable: @chapter, sender: current_user, receiver: @chapter.course.students, action: 'update'
      redirect_to course_chapters_path(@course), notice: 'Chapter was updated'
    else
      render 'edit'
    end
  end

  def edit
    @chapter = @course.chapters.find(params[:id])
  end

  def destroy
    @chapter = @course.chapters.find(params[:id])
    authorize! :destroy, @chapter
    @chapter.destroy
    redirect_to course_chapters_path(@course)
  end

  private

  def chapter_params
    params.require(:chapter).permit(:name, :description, course_attachments_attributes: [:id, :file, :_destroy])
  end

  def load_course
    @course = current_user.my_courses.find_by(id: params[:course_id])
  end

end