class Admin::UsersController < AdminController

  #/users
  def index
    @users = User.all
  end

  #/users/new
  def new
    @user = User.new
  end

  #/users/:id/edit
  def edit
    @user = User.find(params[:id])
  end

  #POST /users
  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to admin_users_path
    else
      render 'new'
    end
  end

  #PATCH /users/:id
  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      redirect_to admin_users_path
    else
      render 'edit'
    end

  end

  #DELETE /users/:id
  def destroy
    @user = User.find(params[:id])
    authorize! :destroy, @user
    @user.destroy
    redirect_to admin_users_path
  end

  private

  #strong params
  def user_params
    params.require(:user).permit(:name, :email, :role, :password, :password_confirmation)
  end
end
