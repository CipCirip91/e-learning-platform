class Admin::DepartmentsController < AdminController

  before_action :load_study_year

  def index
    @departments = @study_year.departments
  end

  def new
    @department = @study_year.departments.build
    authorize! :create, @department
  end

  def edit
    @department = @study_year.departments.find(params[:id])
    authorize! :update, @department
  end

  def create
    @department = @study_year.departments.new(department_params)
    authorize! :create, @department
    if @department.save
      redirect_to admin_study_year_departments_path(@study_year.id), notice: 'Department was created'
    else
      render 'new'
    end
  end

  def update
    @department = @study_year.departments.find(params[:id])
    if @department.update(department_params)
      redirect_to admin_study_year_departments_path(@study_year.id), notice: 'Department was updated'
    else
      render 'edit'
    end
  end

  def destroy
    @department = @study_year.departments.find(params[:id])
    authorize! :delete, @department
    @department.destroy
    redirect_to admin_study_year_departments_path(@study_year.id), notice: 'Department was deleted'
  end

  private

  def department_params
    params.require(:department).permit(:name, courses_attributes: [:id, :title, :description, :_destroy, :study_year_id, student_ids: [], teacher_ids: []])
  end

  def load_study_year
    @study_year = StudyYear.find(params[:study_year_id])
    @available_students = User.students
  end

end
