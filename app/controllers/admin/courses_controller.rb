class Admin::CoursesController < AdminController

  def index
    @courses = Course.includes(:tags)
    @courses = @courses.tagged_with(params[:tag]) if params[:tag].present?
  end

  def new
    @course = Course.new
    authorize! :create, @course
  end

  def edit
    @course = Course.find(params[:id])
    authorize! :update, @course
  end

  def create
    @course = Course.new(course_params)
    authorize! :create, @course
    if @course.save
      redirect_to admin_courses_path, notice: 'Course was created'
    else
      render 'new'
    end
  end

  def update
    @course = Course.find(params[:id])
    if @course.update(course_params)
      redirect_to admin_courses_path, notice: 'Course was updated'
    else
      render 'edit'
    end
  end

  def destroy
    @course = Course.find(params[:id])
    authorize! :delete, @course
    @course.destroy
    redirect_to admin_courses_path, notice: 'Course was deleted'
  end

  private

  def course_params
    params.require(:course).permit(:title, :tag_list, :description)
  end

end
