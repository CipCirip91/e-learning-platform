class Admin::StudyYearsController < AdminController

  def index
    @study_years = StudyYear.all
  end

  def new
    @study_year = StudyYear.new
    authorize! :create, @study_year
  end

  def edit
    @study_year = StudyYear.find(params[:id])
    authorize! :update, @study_year
  end

  def create
    @study_year = StudyYear.new(study_year_params)
    authorize! :create, @study_year
    if @study_year.save
      redirect_to new_admin_study_year_department_path(@study_year), notice: 'Study Year was created'
    else
      render 'new'
    end
  end

  def update
    @study_year = StudyYear.find(params[:id])
    if @study_year.update(study_year_params)
      redirect_to admin_study_years_path, notice: 'Study Year was updated'
    else
      render 'edit'
    end
  end

  def destroy
    @study_year = StudyYear.find(params[:id])
    authorize! :delete, @study_year
    @study_year.destroy
    redirect_to admin_study_years_path, notice: 'Study Year was deleted'
  end

  private

  def study_year_params
    params.require(:study_year).permit(:name, :start_date, departments_attributes: [:id, :name, :_destroy])
  end

end
