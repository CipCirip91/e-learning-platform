class Users::InvitationsController < Devise::InvitationsController

  before_filter :configure_permitted_parameters

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:invite).concat [:name, :role]
  end

end