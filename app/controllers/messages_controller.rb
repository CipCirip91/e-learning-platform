class MessagesController < ApplicationController

  def index
    @messages = current_user.messages.includes(:messageable).order('id desc')
    current_user.update_attribute(:has_notifications, false)
  end

end