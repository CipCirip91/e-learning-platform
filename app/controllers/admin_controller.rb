class AdminController < ApplicationController
  before_action :ensure_admin

  def ensure_admin
    unless current_user.admin?
      flash[:error] = 'Permission Denied'
      redirect_to root_path
    end
  end
end