class CommentsController < ApplicationController

  respond_to :js

  #params = {
  #   object_class: Chapter
  #   object_id: integer
  #   message: string
  #}

  def create
    @commentable = params[:object_class].constantize.find(params[:object_id])
    @comment = Comment.build_from(@commentable, current_user.id, params[:message])
    @comment.save
    if params[:parent_comment_id].present?
      @parent_comment = Comment.find(params[:parent_comment_id])
      @comment.move_to_child_of(Comment.find(params[:parent_comment_id]))
    end
    respond_with @comment
  end

end