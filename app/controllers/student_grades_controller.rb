class StudentGradesController < ApplicationController

  before_action :load_course

  def index
    @students = @course.students
    @student_grades = @course.student_grades.includes(:student).load
  end

  def update
    @student_grade = @course.student_grades.find(params[:id])
    authorize! :update, @student_grade
    @student_grade.update(student_grade_params)
    render nothing: true
  end
  protected

  def load_course
    @course = current_user.my_courses.find_by(id: params[:course_id])
  end

  def student_grade_params
    params.require(:student_grade).permit(:grade)
  end

end