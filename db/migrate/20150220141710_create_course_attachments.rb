class CreateCourseAttachments < ActiveRecord::Migration
  def change
    create_table :course_attachments do |t|
      t.integer :course_id
      t.integer :chapter_id
      t.string :file

      t.timestamps null: false
    end
  end
end
