class CreateStudentGrades < ActiveRecord::Migration
  def change
    create_table :student_grades do |t|
      t.integer :student_id
      t.integer :course_id
      t.text :grade

      t.timestamps null: false
    end
  end
end
