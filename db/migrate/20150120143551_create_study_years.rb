class CreateStudyYears < ActiveRecord::Migration
  def change
    create_table :study_years do |t|
      t.string :name
      t.date :start_date

      t.timestamps null: false
    end
  end
end
