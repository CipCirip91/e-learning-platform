class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.string :title
      t.text :description
      t.integer :department_id
      t.integer :study_year_id

      t.timestamps null: false
    end
  end
end
