class CreateCourseTeacherMemberships < ActiveRecord::Migration
  def change
    create_table :course_teacher_memberships do |t|
      t.integer :course_id
      t.integer :teacher_id

      t.timestamps null: false
    end
  end
end
