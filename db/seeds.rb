@pass = 'admin123'

def make_user(name=nil, email=nil, role='student')
  name  = name || Faker::Name.name
  email = email || Faker::Internet.email(name)
  User.create(
      email: email,
      password: @pass,
      password_confirmation: @pass,
      role: role,
      name: name,
  )
end

#create admin
make_user 'Ciprian', 'deepgreen1991@gmail.com', 'admin'

50.times { make_user nil, nil, 'student'}
10.times { make_user nil, nil, 'prof'}

cursuri = [
    {
        title: 'Criptografie',
        description: "\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'o descriere'
            }
        ]
    }, {
        title: 'Geometrie Diferentiala',
        description: "geometrie\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'materie grea'
            }, {
                name: 'capitol 2',
                description: 'materie si mai grea'
            }
        ]
    }, {
        title: 'Progamare Procedurala',
        description: "progamare in C\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'awesome'
            }
        ]
    }, {
        title: 'Inteligenta Artificiala',
        description: "nasol\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'ce mai e si asta?'
            }
        ]
    },{
        title: '',
        description: "cevaceva\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'o descriere'
            }
        ]
    }, {
        title: 'Statistica',
        description: "cevaceva\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'o descriere'
            },
            {
                name: 'capitol 2',
                description: 'o descriere'
            },
            {
                name: 'capitol 3',
                description: 'o descriere'
            }
        ]
    },{
        title: 'Java',
        description: "Java\n",
        chapters: [
            {
                name: 'capitol 1',
                description: 'Hello there'
            }
        ]
    },{
        title: 'Sisteme de Operare',
        description: "cevaceva\n",
        chapters: [
            {
                name: 'Operating systems introduction',
                description: 'description'
            }
        ]
    },{
        title: 'Algoritmi',
        description: "fain\n",
        chapters: [
            {
                name: 'Sortari',
                description: 'complexitate O(n)'
            }
        ]
    },{
        title: 'Retele',
        description: "tare",
        chapters: [
            {
                name: 'CISCO',
                description: 'Rutare'
            }
        ]
    }
]

study_year = StudyYear.create(
    name: "Informatica 2014-2015",
    start_date: Date.parse('2014-10-1')
)

department_informatica = study_year.departments.create(
    name: 'Informatica'
)

department_matematica = study_year.departments.create(
    name: 'Matematica'
)

cursuri.each do |curs|
  course = Course.new(
      title: curs[:title],
      description: curs[:description],
      department: department_informatica,
      study_year: study_year,
      teachers: User.where(role: 'prof').all.sample(2),
      students: User.where(role: 'student').all.sample(25)
  )

  course.chapters = curs[:chapters].map do |ch|
    Chapter.new(
        name: ch[:name],
        description: ch[:description]
    )
  end

  course.save
end